import moment from 'moment';
import { getDate } from '../helpers/utils';

const API_URL = 'https://edikdolynskyi.github.io/react_sources/messages.json';

const getData = async url => {
  const res = await fetch(url);
  if (!res.ok) {
    throw new Error(`Received ${res.status}`);
  }
  return await res.json();
};

const createMessagesSortedByDate = data => {
  const timeFormat = 'YYYY-MM-DD HH:mm:ss.SSSZ';
  const messages = data.map(message => {
    const { id, text, userId, user, avatar, createdAt } = message;
    const [date, time] = getDate(createdAt);
    return { id, text, userId, user, avatar, createdAt, date, time, isLikedByCurrentUser: false };
  });
  
  return messages
    .sort((firstMessage, secondMessage) => {
      const firstDate = firstMessage.createdAt;
      const secondDate = secondMessage.createdAt;
      return moment(firstDate, timeFormat).diff(secondDate, timeFormat);
    });
};

const createUser = () => {
  return {
    id: '4b003c20-1b8f-11e8-9629-c7eca82aa7bd',
    user: 'Helen',
    avatar: 'https://resizing.flixster.com/PCEX63VBu7wVvdt9Eq-FrTI6d_4=/300x300/v1.cjs0MzYxNjtqOzE4NDk1OzEyMDA7MzQ5OzMxMQ',
    userId: '4b003c20-1b8f-11e8-9629-c7eca82aa7bd'
  };
};

const getMessages = async url => {
  const data = await getData(API_URL);
  const messages = createMessagesSortedByDate(data);
  const currentUser = createUser();
  return { messages, currentUser };
};

export default getMessages;