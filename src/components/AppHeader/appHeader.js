import React from 'react';

import './appHeader.css';

export const AppHeader = () => {
  return (
    <header className="app-header">
      <div className="app-header__logo-wrapper">
        <img className="app-header__logo" src="/logo.png" alt="logo" />
      </div>
      <h1 className="app-header__heading">React chat app</h1>
    </header>
  );
}
