import React from 'react';
import ReactDOM from 'react-dom';
import Chat from './components/Chat'
import './index.css'
import AppHeader from './components/AppHeader';
import AppFooter from './components/AppFooter';

ReactDOM.render(
  <div className="app">
    <AppHeader />
    <Chat />
    <AppFooter />
  </div>
  ,
  document.getElementById('root'))