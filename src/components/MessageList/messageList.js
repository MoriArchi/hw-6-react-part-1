import React, { Component } from 'react';
import PropTypes from 'prop-types';

import Message from '../Message';
import './messageList.css';

export class MessageList extends Component {

  messagesEndRef = React.createRef();

  componentDidMount() {
    this.scrollToBottom();
  }

  componentDidUpdate(prevProps) {
    const { messages: currentMessages } = this.props;
    const { messages } = prevProps;
    if (currentMessages.length > messages.length) {
      this.scrollToBottom();
    }
  }

  scrollToBottom = () => {
    this.messagesEndRef.current.scrollIntoView({ behavior: 'smooth' });
  };

  render() {
    const { currentUserId, messages, onMessageLike, onMessageEdit, onMessageDelete } = this.props;
    let date;
    const messageItems = messages.map(message => {
      const { id, ...messageProps } = message;
      const isSameDate = date === messageProps.date;
      if (!isSameDate) {
        date = messageProps.date;
      }
      const line = (
        <div key={messageProps.date} className="messages__date-line">
          <span>{messageProps.date}</span>
        </div>
      );
      return (
        <React.Fragment key={id}>
          {!isSameDate && line}
          <Message
            currentUserId={currentUserId}
            {...messageProps}
            onLike={() => onMessageLike(id)}
            onDelete={() => onMessageDelete(id)}
            onEdit={() => onMessageEdit({ id, text: messageProps.text })}
          />
        </React.Fragment>
      );
    });
    return (
      <div className="messages">
        {messageItems}
        <div ref={this.messagesEndRef}></div>
      </div>
    );
  }
}

MessageList.propTypes = {
  currentUserId: PropTypes.string.isRequired,
  message: PropTypes.shape({
    text: PropTypes.string.isRequired,
    userId: PropTypes.string.isRequired,
    user: PropTypes.string.isRequired,
    avatar: PropTypes.string.isRequired,
    time: PropTypes.string.isRequired,
    isLikedByCurrentUser: PropTypes.bool.isRequired
  }),
  onMessageLike: PropTypes.func.isRequired,
  onMessageEdit: PropTypes.func.isRequired,
  onMessageDelete: PropTypes.func.isRequired
};
