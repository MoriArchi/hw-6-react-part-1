import React, { Component } from 'react';
import moment from 'moment';
import getMessages from '../../services/messageService';
import { getIndex, createMessage } from '../../helpers/utils';

import Loader from '../Loader';
import Header from '../Header';
import MessageList from '../MessageList';
import MessageInput from '../MessageInput';

import './chat.css';

const timeFormat = 'YYYY-MM-DD HH:mm:ss.SSSZ';

export class Chat extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoaded: false,
      messages: [],
      currentUser: {},
      editMessage: { isEdit: false, id: null, text: null }
    };
  }

  componentDidMount() {
    getMessages().then(
      (data) => {
        console.log(data);
        this.setState({
          messages: data.messages,
          currentUser: data.currentUser,
          isLoaded: true,
        });
      }
    );
  }

  handleMessageLike = id => {
    this.setState(prevState => {
      const messageIndex = getIndex(prevState.messages, id);
      const oldMessage = prevState.messages[messageIndex];
      const newLikeValue = !oldMessage.isLikedByCurrentUser;
      const updatedMessage = { ...oldMessage, isLikedByCurrentUser: newLikeValue };
      const messages = [
        ...prevState.messages.slice(0, messageIndex),
        updatedMessage,
        ...prevState.messages.slice(messageIndex + 1)
      ];
      return { messages };
    });
  };

  handleMessageDelete = id => {
    this.setState(prevState => {
      const messageIndex = getIndex(prevState.messages, id);
      const messages = [...prevState.messages.slice(0, messageIndex), ...prevState.messages.slice(messageIndex + 1)];
      return { messages };
    });
  };

  handleMessageAdd = text => {
    this.setState(prevState => {
      const newMessage = createMessage(text, prevState.currentUser);
      const messages = [...prevState.messages, newMessage];
      return { messages };
    });
  };

  handleMessageEdit = ({ id, text }) => {
    this.setState({ editMessage: { isEdit: true, text, id } });
  };

  onMessageEdit = ({ isEdit, text, id }) => {
    this.setState(prevState => {
      if (!isEdit) {
        return { editMessage: { isEdit: false, text: null, id: null } };
      }
      const messageIndex = getIndex(prevState.messages, id);
      const oldMessage = prevState.messages[messageIndex];
      const updatedMessage = { ...oldMessage, text };
      const messages = [
        ...prevState.messages.slice(0, messageIndex),
        updatedMessage,
        ...prevState.messages.slice(messageIndex + 1)
      ];
      return { messages, editMessage: { isEdit: false, text: null, id: null } };
    });
  };

  render() {
    const { messages, isLoaded, currentUser } = this.state;

    const chatName = 'React chat';
    const partipiantsCount = new Set(messages.map(message => message.user)).size;
    const messagesCount = messages.length;
    const lastMessageTime = messages.length && moment(messages.slice(-1)[0].createdAt, timeFormat).format('Do MMMM HH:mm');

    return !isLoaded ? (
      <Loader />
    ) : (
        <div className="chat__container">
          <Header
            chatName={chatName}
            partipiantsCount={partipiantsCount}
            messagesCount={messagesCount}
            lastMessageTime={lastMessageTime}
          />
          <MessageList
            currentUserId={currentUser.id}
            messages={messages}
            onMessageLike={this.handleMessageLike}
            onMessageDelete={this.handleMessageDelete}
            onMessageEdit={this.handleMessageEdit}
          />
          <MessageInput
            onMessageAdd={this.handleMessageAdd}
            onMessageEdit={this.onMessageEdit}
            editMessage={this.state.editMessage}
          />
        </div>
      );
  }
}