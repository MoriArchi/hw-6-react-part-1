import React from 'react';

import './appFooter.css'

export const AppFooter = () => {
  return (
    <div className="app-footer">
      <div className="app-footer__copyright">
        2020 &#169; Vasyl  Davydov
      </div>
    </div>
  );
}