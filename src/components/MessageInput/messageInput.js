import React, { Component } from 'react';
import PropTypes from 'prop-types';

import './messageInput.css';

export class MessageInput extends Component {
  state = {
    value: '',
    editedMessage: { isEdit: false, id: '' }
  };

  componentDidUpdate(prevProps) {
    if (prevProps.editMessage !== this.props.editMessage) {
      const { isEdit, id, text } = this.props.editMessage;
      this.setState({
        value: text,
        editedMessage: { isEdit, id }
      });
    }
  }

  handleChange = ({ target }) => {
    const value = target.value;
    this.setState({ value });
  };

  handleSubmit = event => {
    event.preventDefault();
    const {
      editedMessage: { isEdit, id },
      value
    } = this.state;
    if (isEdit) {
      const { onMessageEdit } = this.props;
      if (!value) {
        return this.cancelEditingMessage(onMessageEdit);
      } else {
        return this.updateMessage(onMessageEdit, id, value);
      }
    }
    const { onMessageAdd } = this.props;
    this.addMessage(onMessageAdd, value);
  };

  addMessage(onMessageAdd, value) {
    if (!value) {
      return;
    }
    onMessageAdd(value);
    this.setState({
      value: ''
    });
  }

  updateMessage(onMessageEdit, id, value) {
    const editedMessage = { isEdit: true, id, text: value };
    onMessageEdit(editedMessage);
    this.setState({
      value: '',
      editedMessage: { isEdit: false, id: '' }
    });
  }

  cancelEditingMessage(onMessageEdit) {
    onMessageEdit({ isEdit: false });
    this.setState({
      value: '',
      editedMessage: { isEdit: false, id: '' }
    });
  }

  render() {
    const { onMessageEdit } = this.props;
    const {
      editedMessage: { isEdit }
    } = this.state;
    return (
      <div className="message-input">
        <form onSubmit={this.handleSubmit}>
          <textarea
            placeholder="Type your message here..."
            onChange={this.handleChange}
            value={this.state.value}
          ></textarea>
          <div className="message-input__buttons">
            <button type="submit">{isEdit ? 'Update' : 'Send'}</button>
            {this.state.editedMessage.isEdit && (
              <button onClick={() => this.cancelEditingMessage(onMessageEdit)}>Cancel</button>
            )}
          </div>
        </form>
      </div>
    );
  }
}

MessageInput.propTypes = {
  editedMessage: PropTypes.shape({
    isEdit: PropTypes.bool.isRequired,
    id: PropTypes.string.isRequired,
    text: PropTypes.string.isRequired
  }),
  onMessageAdd: PropTypes.func.isRequired,
  onMessageEdit: PropTypes.func.isRequired
};
